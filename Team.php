<?php


namespace App\Model;


class Team
{
    private $id;
    private $title;
    private $points;
    private $city;

    /**
     * Team constructor.
     * @param $id
     * @param $title
     * @param $points
     * @param $city
     */
    public function __construct($id, $title, $points, $city)
    {
        $this->id = $id;
        $this->title = $title;
        $this->poits = $points;
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * @param mixed $points
     */
    public function setPoits($points): void
    {
        $this->poits = $points;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city): void
    {
        $this->city = $city;
    }


}